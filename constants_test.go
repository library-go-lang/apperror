package apperror_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/apperror"
)

func TestConstantsToString(t *testing.T) {
	s := apperror.AppErrorCodeNotFound.String()

	assert.Equal(t, "err_not_found", s, "[TestConstantsToString] Should return \"err_not_found\"")
	assert.IsType(t, "", s, "[TestConstantsToString] return is should string")
}
