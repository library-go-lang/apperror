package apperror_test

import (
	"fmt"
	"testing"

	"github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/apperror"
)

func TestFromMysqlErrorInternalService(t *testing.T) {
	errx := fmt.Errorf("testing")

	err := apperror.FromMysqlError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMysqlErrorInternalService] Should error")
	assert.Equal(t, apperror.ErrInternalServerError(
		"",
		"",
		"",
		nil,
	), err, "[TestFromMysqlErrorInternalService] Should Internal Error")
}

func TestFromMysqlErrForbidden(t *testing.T) {
	errx := &mysql.MySQLError{
		Number: 1045,
	}

	err := apperror.FromMysqlError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMysqlErrForbidden] Should error")
	assert.Equal(t, apperror.ErrForbidden(
		apperror.AppErrorCodeForbidden,
		"",
		"",
		nil,
	), err, "[TestFromMysqlErrForbidden] Should Error Forbidden")
}

func TestFromMysqlErrConflict(t *testing.T) {
	errx := &mysql.MySQLError{
		Number: 1062,
	}

	err := apperror.FromMysqlError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMysqlErrConflict] Should error")
	assert.Equal(t, apperror.ErrConflict(
		apperror.AppErrorCodeDuplicate,
		"",
		"",
		nil,
	), err, "[TestFromMysqlErrConflict] Should Error Forbidden")
}

func TestFromMysqlErrNotFound(t *testing.T) {
	errx := &mysql.MySQLError{
		Number: 1452,
	}

	err := apperror.FromMysqlError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMysqlErrNotFound] Should error")
	assert.Equal(t, apperror.ErrNotFound(
		apperror.AppErrorCodeNotFound,
		"",
		"",
		nil,
	), err, "[TestFromMysqlErrNotFound] Should Error Forbidden")
}
