package apperror_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/apperror"
)

func TestErrorString(t *testing.T) {
	str := apperror.ErrCommandIsRequired.Error()

	assert.Equal(t, "[err_internal_server] Command is required for this action", str, "[TestErrorString] return should \"[err_internal_server] Command is required for this action\"")
	assert.IsType(t, "", str, "[TestErrorString] return is should string")
}
