package apperror_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/apperror"
)

func TestIsNotFoundError(t *testing.T) {
	err := apperror.ErrDataNotFound

	b := apperror.IsNotFoundError(err)
	assert.Equal(t, true, b, "[TestIsNotFoundError] Should return true")
}

func TestIsNotFoundErrorFalse(t *testing.T) {
	b := apperror.IsNotFoundError(nil)
	assert.Equal(t, false, b, "[TestIsNotFoundErrorFalse] Should return false")
}

func TestIsServerError(t *testing.T) {
	err := apperror.ErrInternalServerError("", "", "", nil)

	b := apperror.IsServerError(err)
	assert.Equal(t, true, b, "[TestIsServerError] Should return true")
}

func TestTestIsServerErrorFalse(t *testing.T) {
	b := apperror.IsServerError(nil)
	assert.Equal(t, false, b, "[TestTestIsServerErrorFalse] Should return false")
}
