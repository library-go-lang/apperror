package apperror

import "net/http"

var (
	ErrApp = func(status int, code AppErrorCode, msg, src string, data interface{}) *AppError {
		return &AppError{
			Status:  status,
			Code:    code,
			Message: msg,
			Source:  src,
			Data:    data,
		}
	}
	ErrForbidden = func(code AppErrorCode, msg, src string, data interface{}) *AppError {
		if code == "" {
			code = AppErrorCodeForbidden
		}

		return ErrApp(
			http.StatusForbidden,
			code,
			msg,
			src,
			data,
		)
	}
	ErrBadRequest = func(code AppErrorCode, msg, src string, data interface{}) *AppError {
		if code == "" {
			code = AppErrorCodeBadRequest
		}

		return ErrApp(
			http.StatusBadRequest,
			code,
			msg,
			src,
			data,
		)
	}
	ErrNotFound = func(code AppErrorCode, msg, src string, data interface{}) *AppError {
		if code == "" {
			code = AppErrorCodeNotFound
		}

		return ErrApp(
			http.StatusNotFound,
			code,
			msg,
			src,
			data,
		)
	}
	ErrConflict = func(code AppErrorCode, msg, src string, data interface{}) *AppError {
		if code == "" {
			code = AppErrorCodeDuplicate
		}

		return ErrApp(
			http.StatusConflict,
			code,
			msg,
			src,
			data,
		)
	}
	ErrUnauthorized = func(code AppErrorCode, msg, src string, data interface{}) *AppError {
		if code == "" {
			code = AppErrorCodeUnauthorized
		}

		return ErrApp(
			http.StatusUnauthorized,
			code,
			msg,
			src,
			data,
		)
	}
	ErrUnprocessableEntity = func(code AppErrorCode, msg, src string, data interface{}) *AppError {
		if code == "" {
			code = AppErrorCodeUnprocessableEntity
		}

		return ErrApp(
			http.StatusUnprocessableEntity,
			code,
			msg,
			src,
			data,
		)
	}
	ErrInternalServerError = func(code AppErrorCode, msg, src string, data interface{}) *AppError {
		if code == "" {
			code = AppErrorCodeInternalServerError
		}

		return ErrApp(
			http.StatusInternalServerError,
			code,
			msg,
			src,
			data,
		)
	}
	ErrCommandIsRequired = ErrInternalServerError(
		AppErrorCodeInternalServerError,
		"Command is required for this action",
		"",
		nil,
	)
)
