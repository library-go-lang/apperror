package apperror_test

import (
	"fmt"
	"testing"

	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/apperror"
)

func TestFromPostgreInternalserver(t *testing.T) {
	err := apperror.FromPostgreError(fmt.Errorf("error tes"), "", "", nil)

	assert.Error(t, err, "[TestFromPostgreInternalserver] Should error")
	assert.Equal(t, apperror.ErrInternalServerError(
		"",
		"",
		"",
		nil,
	), err, "[TestFromPostgreInternalserver] Should Internal Error")
}

func TestFromPostgreErrConflict(t *testing.T) {
	errx := &pq.Error{
		Code: "23505",
	}
	err := apperror.FromPostgreError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromPostgreErrConflict] Should error")
	assert.Equal(t, apperror.ErrConflict(
		apperror.AppErrorCodeDuplicate,
		"",
		"",
		nil,
	), err, "[TestFromPostgreErrConflict] Should Internal Error")
}
