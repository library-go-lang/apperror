package apperror_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/apperror"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestFromMongoNotFound(t *testing.T) {
	err := apperror.FromMongoError(mongo.ErrNoDocuments, "", "", nil)

	assert.Error(t, err, "[TestFromMongoNotFound] Should error")
	assert.Equal(t, apperror.ErrNotFound(
		apperror.AppErrorCodeNotFound,
		"",
		"",
		nil,
	), err, "[TestFromMongoNotFound] Should Error Forbidden")
}

func TestFromMongoInternalserver(t *testing.T) {
	err := apperror.FromMongoError(fmt.Errorf("error tes"), "", "", nil)

	assert.Error(t, err, "[TestFromMongoInternalserver] Should error")
	assert.Equal(t, apperror.ErrInternalServerError(
		"",
		"",
		"",
		nil,
	), err, "[TestFromMongoInternalserver] Should Internal Error")
}

func TestFromMongoErrConflict(t *testing.T) {
	errx := mongo.CommandError{
		Code:    11000,
		Message: "Error Duplicate {tes}",
	}
	err := apperror.FromMongoError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMongoErrConflict] Should error")
	assert.Equal(t, apperror.ErrConflict(
		apperror.AppErrorCodeDuplicate,
		"Duplicate {tes",
		"",
		nil,
	), err, "[TestFromMongoErrConflict] Should Internal Error")
}
