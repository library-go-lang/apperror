package apperror_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/library-go-lang/apperror"
)

func TestCreateErrForbidden(t *testing.T) {
	err := apperror.ErrForbidden("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrForbidden] Should error")
	assert.IsType(t, &apperror.AppError{}, err, "[TestCreateErrForbidden] Should App error")
}

func TestCreateErrBadRequest(t *testing.T) {
	err := apperror.ErrBadRequest("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrBadRequest] Should error")
	assert.IsType(t, &apperror.AppError{}, err, "[TestCreateErrBadRequest] Should App error")
}

func TestCreateErrNotFound(t *testing.T) {
	err := apperror.ErrNotFound("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrNotFound] Should error")
	assert.IsType(t, &apperror.AppError{}, err, "[TestCreateErrNotFound] Should App error")
}

func TestCreateErrConflict(t *testing.T) {
	err := apperror.ErrConflict("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrConflict] Should error")
	assert.IsType(t, &apperror.AppError{}, err, "[TestCreateErrConflict] Should App error")
}

func TestCreateErrUnauthorized(t *testing.T) {
	err := apperror.ErrUnauthorized("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrUnauthorized] Should error")
	assert.IsType(t, &apperror.AppError{}, err, "[TestCreateErrUnauthorized] Should App error")
}

func TestCreateErrUnprocessableEntity(t *testing.T) {
	err := apperror.ErrUnprocessableEntity("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrUnprocessableEntity] Should error")
	assert.IsType(t, &apperror.AppError{}, err, "[TestCreateErrUnprocessableEntity] Should App error")
}
